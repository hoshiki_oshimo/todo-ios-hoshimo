//
//  BasicResponse.swift
//  TodoApp
//
//  Created by hoshimo on 2020/08/04.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Foundation

struct BasicResponse: BaseResponse {
    let errorCode: Int
    let errorMessage: String
}
