//
//  RequestProtocol.swift
//  TodoApp
//
//  Created by hoshimo on 2020/07/31.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Alamofire

protocol RequestProtocol {
    associatedtype Response: Codable
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
}

extension RequestProtocol {
    var baseUrl: String {
        return "https://todo-server-hoshimo.herokuapp.com/"
    }
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    var headers: HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }
}
