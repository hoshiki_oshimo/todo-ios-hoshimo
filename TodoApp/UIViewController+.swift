//
//  UIViewController+.swift
//  TodoApp
//
//  Created by hoshimo on 2020/07/31.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorAlertDialog(message: String) {
        let alertController = UIAlertController(title: "エラー", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "閉じる", style: .default))
        present(alertController, animated: true)
    }
}
