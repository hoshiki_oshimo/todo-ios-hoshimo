//
//  TodoRequestBody.swift
//  TodoApp
//
//  Created by hoshimo on 2020/08/06.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Foundation

struct TodoRequestBody {
    let title: String
    let detail: String?
    let date: Date?
}
