//
//  TodoListViewController.swift
//  TodoApp
//
//  Created by hoshimo on 2020/07/21.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import MaterialComponents.MaterialSnackbar

final class TodoListViewController: UIViewController{
    private var isDeleteMode = false

    private var todoList:[Todo] = []

    @IBOutlet private weak var todoTableView: UITableView!
    @IBOutlet private weak var registerButton: UIBarButtonItem!

    @IBAction private func onTappedRegisterButton(_ sender: Any) {
        goToTodoEditView()
    }

    @IBAction private func onTappedTrashButton(_ sender: Any) {
        isDeleteMode.toggle()
        let systemItem: UIBarButtonItem.SystemItem = isDeleteMode ? .stop : .trash
        let deleteButton = UIBarButtonItem(barButtonSystemItem: systemItem, target: self, action: #selector(onTappedTrashButton(_:)))
        navigationItem.rightBarButtonItems = [deleteButton, registerButton]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        todoTableView.dataSource = self
        todoTableView.delegate = self
        navigationItem.backBarButtonItem = UIBarButtonItem()
        fetchTodoList()
    }

    private func fetchTodoList() {
        APIClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                self?.todoList = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] errorMessage in
                self?.showErrorAlertDialog(message: errorMessage)
            }
        )
    }

    private func deleteTodo(id: Int) {
        APIClient().call(
            request: TodoDeleteRequest(id: id),
            success: { [weak self] _ in
                self?.fetchTodoList()
            },
            failure: { [weak self] message in
                self?.showErrorAlertDialog(message: message)
            }
        )
    }

    private func showSnackbar(text: String) {
        let message = MDCSnackbarMessage()
        message.text = text
        MDCSnackbarManager.show(message)
    }

    private func goToTodoEditView(todo: Todo? = nil) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditView = storyboard.instantiateViewController(withIdentifier: "TodoEditView") as? TodoEditViewController else { return }
        todoEditView.delegate = self
        todoEditView.todo = todo
        navigationController?.pushViewController(todoEditView, animated: true)
    }

    private func showDeleteAlertDialog(id: Int, title: String) {
        let alertController = UIAlertController(title: "「\(title)」を削除します。", message: "よろしいですか?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "キャンセル", style: .cancel))
        alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in self.deleteTodo(id: id) })
        present(alertController, animated: true)
    }
}

extension TodoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = todoList[indexPath.row].title
        return cell
    }
}

extension TodoListViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, message: String) {
        showSnackbar(text: message)
        fetchTodoList()
    }
}

extension TodoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let todo = todoList[indexPath.row]

        if isDeleteMode {
            showDeleteAlertDialog(id: todo.id, title: todo.title)
        } else {
            goToTodoEditView(todo: todo)
        }
    }
}
