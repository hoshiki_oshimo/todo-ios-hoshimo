//
//  BaseResponse.swift
//  TodoApp
//
//  Created by hoshimo on 2020/07/31.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Foundation

protocol BaseResponse: Codable {
    var errorCode: Int { get }
    var errorMessage: String { get }
}
