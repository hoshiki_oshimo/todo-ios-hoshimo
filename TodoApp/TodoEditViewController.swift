//
//  TodoEditViewController.swift
//  TodoApp
//
//  Created by hoshimo on 2020/07/27.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import UIKit

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, message: String)
}

final class TodoEditViewController: UIViewController {
    weak var delegate: TodoEditViewControllerDelegate?

    @IBOutlet private weak var detailTextView: UITextView!
    @IBOutlet private weak var registrationButton: UIButton!
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var titleTextLabel: UILabel!
    @IBOutlet private weak var detailTextLabel: UILabel!
    @IBOutlet private weak var dateTextField: UITextField!

    var todo: Todo?
    private let titleMaxLimit = 100
    private let detailMaxLimit = 1000
    private let datePicker = UIDatePicker()
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/M/d"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter
    }()

    private var detailTextCharacter: String {
        detailTextView.text ?? ""
    }
    private var titleTextCharacter: String {
        titleTextField.text ?? ""
    }
    private var isDetailTextOverLimit: Bool {
        detailTextCharacter.count > detailMaxLimit
    }
    private var isTitleTextOverLimit: Bool {
        titleTextCharacter.count > titleMaxLimit
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDetailTextView()
        setUpDatePicker()
        setUpRegistrationButton()
        setUpView()
        setUpRegistrationButtonTitle()
        if let todo = todo {
            setUpText(todo: todo)
            setUpTextCounter(todo: todo)
        }
    }

    private func setUpRegistrationButton() {
        registrationButton.layer.cornerRadius = 10.0
        registrationButton.setTitleColor(.white, for: .normal)
        changeRegistrationButtonState()
    }

    private func setUpDetailTextView() {
        detailTextView.delegate = self
        detailTextView.layer.borderWidth = 1.0
        detailTextView.layer.cornerRadius = 10.0
        detailTextView.layer.borderColor = UIColor.lightGray.cgColor
    }

    private func changeRegistrationButtonState() {
        if titleTextCharacter.isEmpty || isTitleTextOverLimit || isDetailTextOverLimit {
            registrationButton.backgroundColor = .gray
            registrationButton.isEnabled = false
        } else {
            registrationButton.backgroundColor = .blue
            registrationButton.isEnabled = true
        }
    }

    private func setUpText(todo: Todo) {
        titleTextField.text = todo.title
        detailTextView.text = todo.detail
        if let date = todo.date {
            dateTextField.text = dateFormatter.string(from: date)
        }
    }

    private func setUpTextCounter(todo: Todo) {
        titleTextLabel.text = String(todo.title.count)
        detailTextLabel.text = String(todo.detail?.count ?? 0)
    }

    private func setUpRegistrationButtonTitle() {
        let message = todo?.id == nil ? "登録" : "更新"
        registrationButton.setTitle(message, for: .normal)
    }

    @IBAction private func onTappedRegistrationButton(_ sender: Any) {
        guard let title = titleTextField.text else { return }
        let detail = detailTextView.text.isEmpty ? nil : detailTextView.text
        let dateText = dateTextField.text ?? ""
        let date = dateFormatter.date(from: dateText)
        if let id = todo?.id {
            let todoUpdateRequest = Todo(id: id, title: title, detail: detail, date: date)
            updateTodo(todoUpdateRequest: TodoUpdateRequest(todo: todoUpdateRequest))
        } else {
            let todoCreateRequest = TodoRequestBody(title: title, detail: detail, date: date)
            createTodo(todoPostRequest: TodoPostRequest(body: todoCreateRequest))
        }
    }

    private func createTodo(todoPostRequest: TodoPostRequest) {
        APIClient().call(
            request: todoPostRequest,
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidSendTodo(self, message: "登録しました")
                self.navigationController?.popViewController(animated: true)
            },
            failure: { [weak self] errorMessage in
                self?.showErrorAlertDialog(message: errorMessage)
            }
        )
    }

    private func updateTodo(todoUpdateRequest: TodoUpdateRequest) {
        APIClient().call(
            request: todoUpdateRequest,
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidSendTodo(self, message: "更新しました")
                self.navigationController?.popViewController(animated: true)
            },
            failure: { [weak self] errorMessage in
                self?.showErrorAlertDialog(message: errorMessage)
            }
        )
    }

    private func setUpDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = Locale.current
        dateTextField.inputView = datePicker
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 45))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let decisionBarButtonItem = UIBarButtonItem(title: "決定", style: .done, target: self, action: #selector(onTappedDecisionBarButton))
        let deleteBarButtonItem = UIBarButtonItem(title: "削除", style: .plain, target: self, action: #selector(onTappedDeleteBarButton))
        let closeBarButtonItem = UIBarButtonItem(title: "閉じる", style: .plain, target: self, action: #selector(onTappedCloseBarButton))

        toolbar.setItems([deleteBarButtonItem, flexibleItem, closeBarButtonItem, decisionBarButtonItem], animated: true)
        dateTextField.inputAccessoryView = toolbar
    }

    private func setUpView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }

    @objc private func onTappedDecisionBarButton() {
        dateTextField.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/M/d"
        dateTextField.text = formatter.string(from: datePicker.date)
    }

    @objc private func onTappedDeleteBarButton() {
        dateTextField.text = ""
        dateTextField.endEditing(true)
    }

    @objc private func onTappedCloseBarButton() {
        dateTextField.endEditing(true)
    }

    @IBAction private func onChangedTitleTextCharacter(_ sender: Any) {
        titleTextLabel.text = String(titleTextCharacter.count)
        titleTextLabel.textColor = isTitleTextOverLimit ? .red : .black
        changeRegistrationButtonState()
    }
}

extension TodoEditViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        detailTextLabel.text = String(detailTextCharacter.count)
        detailTextLabel.textColor = isDetailTextOverLimit ? .red : .black
        changeRegistrationButtonState()
    }
}
