//
//  Todo.swift
//  TodoApp
//
//  Created by hoshimo on 2020/07/31.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Foundation

struct Todo: Codable {
    let id: Int
    let title: String
    let detail: String?
    let date: Date?
}
