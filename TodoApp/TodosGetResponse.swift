//
//  TodosGetResponse.swift
//  TodoApp
//
//  Created by hoshimo on 2020/07/31.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Foundation

struct TodosGetResponse: BaseResponse {
    let todos: [Todo]
    let errorCode: Int
    let errorMessage: String
}
