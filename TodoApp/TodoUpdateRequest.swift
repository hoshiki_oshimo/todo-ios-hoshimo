//
//  TodoUpdateRequest.swift
//  TodoApp
//
//  Created by hoshimo on 2020/08/07.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Alamofire

struct TodoUpdateRequest: RequestProtocol {
    typealias Response = BasicResponse

    let todo: Todo

    var path: String {
        return "todos/\(todo.id)"
    }
    var method: HTTPMethod {
        return .put
    }
    var parameters: Parameters? {
        var parameters = ["title": todo.title, "detail": todo.detail, "date": nil]
        if let date = todo.date {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.timeZone = TimeZone(identifier: "JST")
            parameters["date"] = formatter.string(from: date)
        }
        return parameters as Parameters
    }
}
