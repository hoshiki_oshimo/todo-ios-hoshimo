//
//  TodoDeleteRequest.swift
//  TodoApp
//
//  Created by hoshimo on 2020/08/11.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Alamofire

struct TodoDeleteRequest: RequestProtocol {
    typealias Response = BasicResponse

    let id: Int
    var path: String {
        return "todos/\(id)"
    }
    var method: HTTPMethod {
        return .delete
    }
    var parameters: Parameters? {
        return nil
    }
}
