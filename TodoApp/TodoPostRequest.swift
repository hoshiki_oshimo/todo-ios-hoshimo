//
//  TodoPostRequest.swift
//  TodoApp
//
//  Created by hoshimo on 2020/08/05.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Alamofire

struct TodoPostRequest: RequestProtocol {
    typealias Response = BasicResponse

    let body: TodoRequestBody
    var path: String {
        return "todos"
    }
    var method: HTTPMethod {
        return .post
    }
    var parameters: Parameters? {
        var parameters = ["title": body.title]
        if let detail = body.detail {
            parameters["detail"] = detail
        }
        if let date = body.date {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.timeZone = TimeZone(identifier: "JST")
            parameters["date"] = formatter.string(from: date)
        }
        return parameters
    }
}
