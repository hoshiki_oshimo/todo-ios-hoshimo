//
//  TodosGetRequest.swift
//  TodoApp
//
//  Created by hoshimo on 2020/07/31.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse
    var parameters: Parameters? {
        return nil
    }
    var path: String {
        return "todos"
    }
    var method: HTTPMethod {
        return .get
    }
}
