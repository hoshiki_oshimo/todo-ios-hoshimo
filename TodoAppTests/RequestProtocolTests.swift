//
//  RequestProtocolTests.swift
//  TodoAppTests
//
//  Created by hoshimo on 2020/11/04.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class RequestProtocolTests: XCTestCase {
    func testInit() {
        let request = TestRequestProtocol()

        XCTAssertEqual(request.baseUrl, "https://todo-server-hoshimo.herokuapp.com/")
        XCTAssertEqual(request.path, "todos")
        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.encoding.toJsonEncoding(), .default)
        XCTAssertEqual(request.headers?["Content-Type"], "application/json")
        XCTAssertNil(request.parameters)
    }
}
