//
//  TestRequestProtocol.swift
//  TodoAppTests
//
//  Created by hoshimo on 2020/11/04.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Alamofire
@testable import TodoApp

final class TestRequestProtocol: RequestProtocol {
    typealias Response = TodosGetResponse

    var baseUrl: String {
        return "https://todo-server-hoshimo.herokuapp.com/"
    }

    var path: String {
        return "todos"
    }

    var method: HTTPMethod {
        return .get
    }

    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }

    var headers: HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }

    var parameters: Parameters? {
        return nil
    }
}

extension ParameterEncoding {
    func toJsonEncoding() -> JSONEncoding? {
        return self as? JSONEncoding
    }
}

extension JSONEncoding: Equatable {
    public static func == (lhs: JSONEncoding, rhs: JSONEncoding) -> Bool {
        return lhs.options == rhs.options
    }
}
