//
//  TodoPostRequestTests.swift
//  TodoAppTests
//
//  Created by hoshimo on 2020/11/02.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoPostRequestTests: XCTestCase {
    let postRequest = TodoRequestBody(title: "study", detail: nil, date: nil)
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoPostRequest(body: postRequest)
        XCTAssertEqual(request.method, .post)
        XCTAssertEqual(request.path, "todos")
    }

    func testResponse() {
        var todoPostResponse: BasicResponse?

        stub(condition: isHost("todo-server-hoshimo.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("SuccessResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Post Todo Request")

        APIClient().call(
            request: TodoPostRequest(body: postRequest),
            success: { response in
                todoPostResponse = response
                expectation.fulfill()
        }, failure: { _ in
                return
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoPostResponse)
            XCTAssertEqual(todoPostResponse?.errorCode, 0)
            XCTAssertEqual(todoPostResponse?.errorMessage, "")
        }
    }
}
