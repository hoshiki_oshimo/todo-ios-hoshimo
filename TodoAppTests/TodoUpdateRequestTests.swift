//
//  TodoUpdateRequestTests.swift
//  TodoAppTests
//
//  Created by hoshimo on 2020/11/02.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoUpdateRequestTests: XCTestCase {
    let updateRequest = Todo(id: 1, title: "hello", detail: nil, date: nil)
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoUpdateRequest(todo: updateRequest)
        XCTAssertEqual(request.method, .put)
        XCTAssertEqual(request.path, "todos/1")
    }

    func testResponse() {
        var todoUpdateReponse: BasicResponse?

        stub(condition: isHost("todo-server-hoshimo.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("SuccessResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Update Todo Request")

        APIClient().call(
            request: TodoUpdateRequest(todo: updateRequest),
            success: { response in
                todoUpdateReponse = response
                expectation.fulfill()
        },
            failure: { _ in
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoUpdateReponse)
            XCTAssertEqual(todoUpdateReponse?.errorCode, 0)
            XCTAssertEqual(todoUpdateReponse?.errorMessage, "")
        }
    }
}
