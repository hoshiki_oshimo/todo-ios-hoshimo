//
//  TodoGetRequestTests.swift
//  TodoAppTests
//
//  Created by hoshimo on 2020/11/04.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoGetRequestTests: XCTestCase {
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodosGetRequest()

        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.path, "todos")
    }

    func testResponse() {
        var todosGetResponse: TodosGetResponse?

        stub(condition: isHost("todo-server-hoshimo.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("TodosGetResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Get Todo Request")

        APIClient().call(
            request: TodosGetRequest(),
            success: { response in
                todosGetResponse = response
                expectation.fulfill()
        }, failure: { _ in
                return
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todosGetResponse)
            XCTAssertEqual(todosGetResponse?.errorCode, 0)
            XCTAssertEqual(todosGetResponse?.errorMessage, "")
            XCTAssertEqual(todosGetResponse?.todos.count, 3)
            XCTAssertEqual(todosGetResponse?.todos[0].title, "テスト")
            XCTAssertNil(todosGetResponse?.todos[0].detail)
            XCTAssertNil(todosGetResponse?.todos[0].date)
            XCTAssertEqual(todosGetResponse?.todos[1].title, "テスト2")
            XCTAssertEqual(todosGetResponse?.todos[1].detail, "テスト2")
            XCTAssertNil(todosGetResponse?.todos[1].date)
            XCTAssertEqual(todosGetResponse?.todos[2].title, "テスト3")
            XCTAssertEqual(todosGetResponse?.todos[2].detail, "テスト3")
            XCTAssertEqual(todosGetResponse?.todos[2].date?.toString(), "2020-11-04T00:00:00.000Z")
        }
    }
}

extension Date {
    func toString() -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.timeZone = TimeZone(identifier: "JST")
        return formatter.string(from: self)
    }
}
