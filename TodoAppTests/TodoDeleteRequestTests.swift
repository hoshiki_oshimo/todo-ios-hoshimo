//
//  TodoDeleteRequestTests.swift
//  TodoAppTests
//
//  Created by hoshimo on 2020/11/02.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoDeleteRequestTests: XCTestCase {
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoDeleteRequest(id: 1)
        XCTAssertEqual(request.method, .delete)
        XCTAssertEqual(request.path, "todos/1")
    }

    func testResponse() {
        var todoDeleteReponse: BasicResponse?

        stub(condition: isHost("todo-server-hoshimo.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("SuccessResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Delete Todo Request")

        APIClient().call(
            request: TodoDeleteRequest(id: 1),
            success: { response in
                todoDeleteReponse = response
                expectation.fulfill()
        },
            failure: { _ in
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoDeleteReponse)
            XCTAssertEqual(todoDeleteReponse?.errorCode, 0)
            XCTAssertEqual(todoDeleteReponse?.errorMessage, "")
        }
    }
}
